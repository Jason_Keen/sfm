from pylab import *
from camera import Camera
from sfm import *
n=8
#X=vstack((random.randn(3,n)*5,ones((1,n))))
X=array([[2,10,15,30,3,5,6,7],[3,0,20,30,20,10,30,40],[1,20,10,20,2,3,4,5],[1,1,1,1,1,1,1,1]],dtype=float)
P1=array([[1,0,0,0],[0,1,0,0],[0,0,1,0]],dtype=float)
P2=array([[1,0,0,0],[0,1,0,0],[0,0,1,10]],dtype=float)
c1=Camera(P1)
c2=Camera(P2)
x1=c1.project(X)
x2=c2.project(X)
print "x1"
print x1
print "x2"
print x2
K=array([[1,0,0],[0,1,0],[0,0,1]])
x1n=dot(inv(K),x1)
x2n=dot(inv(K),x2)
print "x1n"
print x1n
print "x2n"
print x2n
E=compute_fundamental_normalized(x1n, x2n)
print "Essential Matrix"
print E
print "|x2n^T*E*x1n|"
print x1n
print x2n
for i in range(8):
    print dot(dot(x2n[:,i].T,E),x1n[:,i])
print "8888888888"
f=E[2,2]
print f
for i in range(8):
    print dot(dot(x2n[:,i].T,E/E[2,2]),x1n[:,i])
P2_=compute_P_from_essential(E)
print "compute_P_from_essential"
print P2_
print "choosed P"
P2_C=choose_P(x1n, x2n, P1, P2_)
print P2_C
X_=triangulate(x1n, x2n, P1, P2_C)
print "X"
print X_
print "error"
print norm(x1-c1.project(X_))
c3=Camera(P2_C)
print norm(x2-c3.project(X_))
print norm(X-X_)
print norm(P2-P2_C)

